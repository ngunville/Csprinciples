def sum(num_list):
    """
        >>> q2(range(1, 12))
        66
    """
def q2(num_list):
    sum = 0
    for number in num_list:
        sum = sum + number
    return(sum)

if __name__ == "__main__":
    import doctest
    doctest.testmod()


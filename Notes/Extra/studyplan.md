Question One: How is the APCS exam scored? How many questions do you have to get right out of 70 to get a 3, 4, or 5? What is the "Create Performance Task" and how is it scored?
    The APCS exam is a final test that is administered at the end of the year. It has 70 questions and it is 70 percent of the main exam grade. The other 30 percent is the "Create Performance Task." This is a task in which you have to collaberate and make a code that does something. You have to be given at least 12 hours of class time to work on it. You can work on it with a group or alone. There are three parts. The first part is the code. You have to make a code alone or with a group. The second step is the video, you have to film yourself explaining how to code works. The last step is answering the questions that are given to you. You will know the questions beforehand so you can learn what you need to know to answer the questions. 
    This is what will give you your grade. The third step is the most important one. 
    The distrabution of the grades is 
    5: 12.4
    4: 21.7
    3: 32.5
    2: 19.9
    1: 13.6
I could not find out how many questions you need to get right to get a 3,4, or 5, but the majority of kids get a 3 every year. 

Question Two: What section did you do the best on, What section do you need to improve upon? I did the best on section 4. I got 4/6 which is a 66 percent. I got 4/11 right on big idea one and this is a 36%. 
Question Three: What are some online rescources that you can use to prepare for each of the Big Ideas? One [link](https://apstudents.collegeboard.org/courses/ap-computer-science-principles/assessment-tips) that I found from the AP collage board talks about the test more generally. 
The [Big Idea One](https://library.fiveable.me/ap-comp-sci-p/big-idea-1) link that you could use talks about what is nessesary to study for this portion.
The Big Idea Two is all about data. Click [Here](https://library.fiveable.me/ap-comp-sci-p/big-idea-2/intro-big-idea-2-data-binary-numbers/study-guide/YlNeQAM5snCDFEtp0CGd) to study.
The Third Big Idea is weighted the most out of any of them because it has the most questions. Click [Here](https://library.fiveable.me/ap-comp-sci-p/ap-cram-sessions-2021/-ap-comp-sci-p-cram-review-big-idea-3-algorithms-and-programming/watch/1V1bP3VbW7bpSMvhi3pR) to study.
Big Idea Four has the least amount of questions with only 6. [Study Guide](https://library.fiveable.me/ap-comp-sci-p/ap-cram-sessions-2021/-ap-comp-sci-p-cram-review-big-idea-4-computer-systems-and-networks/watch/NNevoLYbIM4lYiTCMxm8)
Big Idea Five has 13 questions. [study guide](https://library.fiveable.me/ap-comp-sci-p/ap-cram-sessions-2021/-ap-comp-sci-p-cram-review-big-idea-5-impact-of-computing/watch/Rj7huhqXnHVFS6mK6k59)
These are the tools to study for each of the big ideas of Computer Science Principles.


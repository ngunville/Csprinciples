# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
>
The purpose of this code is to have a player play a game about robots. The player controls a cicle that tries to avoid robots. 
>
2. Describes what functionality of the program is demonstrated in the
   video.
>
The video shows that user playing the game and defeating the robots in one level. In the next level, the robots hit the player and the game ends. When a robot hits another robot, they turn black and stop moving. This acts like a barrier that stops any incoming robots. 
>
3. Describes the input and output of the program demonstrated in the
   video.
>
The input of this code is the keys being pressed. The output is seeing the player moving on the screen and the robots following. 
>
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
>
![list](Shot4.png)
This list stores data about they keys that could be pressed to move the player. It makes it so the player has three diffrent options of keys. All of the charecters in the list, when pressed, do the same thing. 
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
>
![list2](Shot5.png)
The picture above shows what happens if you press either of the keys in the list. This transfers the data from the list and uses the keys being pressed to move the player in the right direction
>
## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
>
The name of this list is keys. The elif statment says that if any of the keys in the keys list are pressed, move the player. 
>
2. Describes what the data contained in the list represent in your
   program
>
The data in this list represents all of the three possible keys that you could press to get the player to move down and left. It is saying that, if any of these keys are pressed, move the player. 
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>
If this program did not contain this list, you could write three seperate "elif" statements for each of they keys. A similar version of this list is used in multiple places all over the code. Overall, if this list was written diffrently, this function would be three times longer. 
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>
![parameter](Shot7.png)
One code segment is called "move player".and example of paremeters in this procedure is, on line 57, the code will only run if the key is pressed and the x coordinate of the player is greater than 0. This is a parameter. If the code does not meet the parametes, the player will not move. 
>
![iteration](Shot8.png)
Iteration is involved when move player is ran multiple times. For instance, in line 175 and 176, is says that, if the game is still running, run the procedure named "move_player"
>
![selection](Shot9.png)
One exmplle of Selection is in line 56-60, it says that, if these keys are pressed, run this action. 
>
![Sequencing](Shot0.png)
Sequencing is shown during lines 168 to 186. All of the functions have been created at this point and these lines are showing what order to run all of the previous functions. 
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
>
One student developed procedure that is called is (place_player)
![procedure](Shot6.png)
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>
This procedure is called at the very start of the program where everythiong is being set up. It assigns random x's and y's to the player and those are used as coordinates for where to place the circle.  
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>
First, assign a random number from 0 to 63 to player.x and a random number from 0 to 47 to player.y. 
Now you have the coordinates on the screen for your player and you use those in another procedure to place it. 
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>
The function "safely place player" is called first when the player enters the screen. It makes sure that the player dosen't immediatly hit a robot upon starting. 
>
>
> Second call:
>
"Safely place player" is called a second time whenever the player teleports. 
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
>
The condition "if player y = robot y and player x = robot x" is tested at the very start of the game is used to see if you will need to teleport the player to a diffrent place. 
>
>
### Condition(s) tested by the second call:
>
For the second call, whenever the robot teleports, the condition is the same and the player will teleport again if it lands on a robot. Landing on a robot in this case will just cause the player to mave to another space and not end the game. 
>
>
3. Identifies the result of each call
>
### Result of the first call:
>
The player is somewhere on the screen and the robots are somewhere else on the screen, "not in the same place as the player."
>
>
### Result of the second call:
>
Whenever the player teleports, it teleports to a space that does not contain a robot. 
>
>

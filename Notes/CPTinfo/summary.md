# Summary
## Program Purpose and Function
### You can get a 0 or a 1 on this section
There must be a video that shows the program running 
It must include
- input
- program functionality
- output
The written response must contain...
- a description about the purpose of the program
- a description about the functionality of the program(talked about in the video)
- a description of the input and output(talked about in the video)
### What would not get you the point
1. The video does not demonstrate the program(no screenshots)
2. The purpose does not adress the problem 
## Data Abstraction
### You can get a 0 or a 1 on this section
The written response must have two program code segments
1. shows how data has been stored in a collection type
2. shows that the data is the same list that is being used
It also must...
- Identify the name of the varible that represents the list that is used
- Describe what the data is representing

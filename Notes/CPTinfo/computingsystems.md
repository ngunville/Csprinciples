# Big Idea 4: Computing Systems and Networks
## Vocabulary
- Bandwidth: a measure of the data transfer rate or capacity of a given network
- Computing device: A functional unit that can perform substantial computations, including numerous arithmetic operations and logic operations without human intervention
- Computing network:  interconnected computing devices that can exchange data and share resources with each other
- Computing system: an implementation of the Network Computing Architecture that distributes computer processing tasks across resources in either a single network or several interconnected networks 
- Data stream: data that is generated continuously by thousands of data sources, which typically send in the data records simultaneously, and in small sizes
- Distributed computing system: consists of multiple software components that are on multiple computers, but run as a single system
- Fault-tolerant: the ability of a system to continue operating without interruption when one or more of its components fail
- Hypertext Transfer Protocol (HTTP): an application protocol for distributed, collaborative, hypermedia information systems that allows users to communicate data on the World Wide Web.
- Hypertext Transfer Protocol Secure (HTTPS): a combination of the Hypertext Transfer Protocol (HTTP) with the Secure Socket Layer (SSL)/Transport Layer Security (TLS) protocol
- Internet Protocol (IP) address: a series of numbers that identifies any device on a network
- Packets: a small segment of a larger message
- Parallel computing system: the process of performing computational tasks across multiple processors at once to improve computing speed and efficiency
- Protocols: an established set of rules that determine how data is transmitted between different devices in the same network
- Redundancy: The act of happening again and again, can be positive or negative
- Router: a networking device that connects computing devices and networks to other networks

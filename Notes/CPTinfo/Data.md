Vocab For Data Big Idea
Abstraction: the process of taking away or removing characteristics from something inorder to reduce it to a set of essential characteristics. 
Analog Data: Data that is represented in a physical way, like the grooves on a cd, non digital.
Bias: systamaticaly and unfairly descriminate against a certain group of individuals in favor of others. 
Binary Number System: A numbering scheme where there are only two possible digits, 1 and 0, used by computers.
Bit: The smallest incrament of data on a computer, either 1 or 0
Byte: A unit of data eight binary digits long 
Classifying Data: The process of orginizing data by relavant catagories so it can be used more efficientaly. 
Cleaning Data: The process of identifying the incorrect, incomplete, inaccurate, irrelevant, or missing part of the data and then changing it. 
Digital Data: an electronic representation of information in a format machines can understand.
Filtering Data: The process of examening a dataset to change it according to certain criteria. 
Information: stimuli that has meaning in some context for it's reciver.
Lossless Data Compression: A way of storing data that does not lose anything in the process. 
Metadata: Data that provides information about other data. summarizes basic info about data.
Overflow Error: when the data type used to store data is not large enough to store the data. 
Patterns in Data: a set of data that follows a recognizable form, which can be used to find something in the data.
Round-off or Rounding Error: The diffrence between an approximation and its exact number. 
Scalability: The measure of a system's ability to increase or decrease in performance and cost in response to changes in application. 

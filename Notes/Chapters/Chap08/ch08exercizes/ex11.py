product = 1  # Start with multiplication identity
d = 1
while d < 11:
    product = product * d
    d = d + 1
print(product)


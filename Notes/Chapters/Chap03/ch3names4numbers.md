# Chapter 1: Names for Numbers
## Assigning A Name
###You can assign a name to a number in computers. One example of this is a varible. A varible is any number that changes throughout the game/anything. Varibles can change over the course of whatever you are doing. 
## What You Can Use as a Varible Name
###There are some restrictions to what can be a varible
###1. It has to start with a letter(A), or and underscore(_).
###2. It can contain common digts like numbers.
###3. It can't be a python keyword like
    ###- and
    ###- def
    ###- elif
    ###- else
    ###- for
    ###- import
    ###- if
###4. Case does not matter. 
###Caramel-casing/Mixed-casing is making the first letter of each word capitilized.
###This is not always the right way to name varibles. In Python, Underscores are the regular way to do this. The Python Style Guide is [Here!](https://peps.python.org/pep-0008/)
## Expressions
###The right hade side of the assignment statement can tell you what the varible will be set to. The left side is what the varible is named. When you run the code and it has a left and right part, the result will be that the varible is set to the solution of the left equation.
##This also applies to all mathmatical symbols. 
###The program that the book runs converts divided numbers into decimals automaticaly, but older versions did not, so you had to put 2.0/4.0 instead of 2/4.If you want the same to be true in Python Three, use the // operator. This will make the solution a whole number.  
## Modulo
###The Modulo symbol is % and is used to find the remainder of numbers when they are divided. This is a helpful tool to find out weather two number can divide into each other evenly. [^1]
###[^1]: If you try this command with an equation that is a smaller number % a bigger number, the solution is always that smaller number because it goes into the bigger number 0 times with and remainder of itself. 
## Summary of Expression Types
###- Expression= Arithmetic meaning
###- 1+2= Addition
###- 3*4= Multiplication
###- 1/3= Integer Division(In this Python code, the result would be .333, but in older codes, it would be 0
###- 2.0/4.0= Division(by adding the decimal points, you are telling the machine to give a decimal answer. 
###- 1//3= Floor division(The answer is 0 on Python 3)
###- -1= Negation(It makes any number negative)
###- 2 % 3= Modulo(This calculates the remainder of this division problem)
###Expressions are evaluated by using PEMDAS and commas are used to seperate equations. If there are two of the same signs, go left to right. 
###You can use parenthesies to tell the computer what order you want it to say things in. 
#Part Two 
###you can do more things that the computer can compute.
###|Operator|Name|
###|-x|Negation|
###|x*y|Multiplication|
###|x/y|Division|
###|x%y|Modulo|
###|x-y|Subtraction|
###|x+y|Addition|
###For instance, you can calculate things like...
result = 4 + -2 * 3
print(result)
###This will print the answer to the question or -2
###You can change the order of the equation by using parentesies just like regular PEMDAS.
###There are diffrent types of code including tracing programs. Tracing programs run each step and then stop. This is diffrent from the regular program that the computer runs because that runs it as fast as possible. This program could be usefull if you wanted to check each step and see where the program is falling apart. 
###a very useful tip is that if you put something( a varible) that is being printed in double quotes, it will print those exact charecters not what the varible equals. 
###The katchup code is bacisally saying that according to the magizine, it drops at 2.464 feet every minute.
###when you make the feet 4, the minutes go up by .6 ish.
###Next, there is a code that is out of order. I solved it(first try) by seeing what varible had to be in place to run each block of code. The first on had to have 0 varibles and was totally self-relient.The second one needed varibles from the prior bolck to do the calculations and so on...
### the order in which you assighn varibles is very important. Varibles are not set to anything and are open to change. 
### the varible that the computer uses is the last one that was typed into the equation. 
###for instance...
var1 = 45
var1 = 17
print(var1)
###The program will write var1 as 17 because that is the last one that I typed. 
###The question at the end of the section said that you have to add 20% to a bill as a tip and then you can add then together and divide them by the number of people. 
app = 2
ent = 9.89
des = 7.99
bill = (10*app) + (10*ent) + des
print(bill)

###This is a program that calculates the cost of a dinner. 
###some varibles are unnessasry but make the program easier to understand. You can also just change the varible throught the code. 
###It is very important to use varible names that are easy to undersand so when you go back and improve it, i=you cna understand what you wrote. 
apples = 4
pears = 3
total_cost = (.4*4)+(.65*3)
print(total_cost)
###This calculates the cost of all of the apples and pears.
paperclip = .05
budget = 4
amount_of_paperclips = 4/.05
print(amount_of_paperclips)
###This command calculates the amount of paperclips you can buy with 4 dollars.
#Summary
##In summary, these are the things that you need to know
### an arthemetic expression includes a mathmatical symbol
###To assign something is to make a varible and tell the computer what it is equal to.
###Assighment Deslexia is when the equation is before the varible and this will not work
###Integer division is // symbol and it gives you an integer version of the answer
###The modulo gives you the remainder of any division equation
###A tracing program is one that runs step by step and show the change in values
###a varible in a name of anything on the other side of the = sign, this cna save a lot of writing


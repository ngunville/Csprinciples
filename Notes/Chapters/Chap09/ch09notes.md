### Chapter 9
## Using Repetition with Strings
Python has the ability to use strings.
A string is a collection of letter, digits, and other charecters. 
Addition symbols put two strings together.
There is such thing as an accumulator pattern. There are five steps to this pattern. 
1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
2. Get all the data to be processed.
3. Step through all the data using a for loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator.
5. Do something with the result.
 
This is an example of this in work.
```

# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "Rubber baby buggy bumpers."

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = new_string + letter

# Step 5: Process result
print(new_string)

```
## Reversing Text
This next example has only a small change, using two strings, but the result is very diffrent.

```

# Step 1: Initialize accumulators
new_string_a = ""
new_string_b = ""

# Step 2: Get data
phrase = "Happy Birthday!"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string_a = letter + new_string_a
    new_string_b = new_string_b + letter

# Step 5: Process result
print("Here's the result of using letter + new_string_a:")
print(new_string_a)
print("Here's the result of using new_string_b + letter:")
print(new_string_b)

```

The first string writes the string backwards. This is because each new letter gets added at the beggining. This creates a reversal.

A palindrome is something that writes the same backward as forward. An example is Racecar.

### Mirroring Text

This next example shows what you can do if you add a letter to both sides of the string that was created. 

```
# Step 1: Initialize accumulator
new_string = ""

# Step 2: Get data
phrase = "This is a test"

# Step 3: Loop through the data
for letter in phrase:
    # Step 4: Accumulate
    new_string = letter + new_string + letter

# Step 5: Process result
print(new_string)
```
When you start with a charecter that wouuld be the middle and the sides will be the phrase backwards and forwards. 

You can use a for loop to repeat until the phrase has been typed. 
The letter in the middle is called an accumulator.
## Modifying Text
Sometimes you will need to change every number to a diffrent number in a string of text. You will use a while loop to do this. This is an example of this example.
```

a_str = "Th1s is a str1ng"
pos = a_str.find("1")
while pos >= 0:
    a_str = a_str[0:pos] + "i" + a_str[pos+1:len(a_str)]
    pos = a_str.find("1")
print(a_str)

```

This will also work when you want to change entire words in text. You can also use a while loop to assign letters to other letters. This creates a coded message. 

## Summary
Accumulator Pattern - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to reverse the characters in a string.
Palindrome - A palindrome has the same letters if you read it from left to right as it does if you read it from right to left. An example is "A but tuba".
String - A string is a collection of letters, numbers, and other characters like spaces inside of a pair of single or double quotes.
# Key Words
def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.
for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
print - The print statement in Python will print the value of the items passed to it.
range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start, stop, step) it returns all the numbers from start to one less than end changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].
while - A while loop is a programming statement that tells the computer to repeat a statement or a set of statements. It repeats the body of the loop while a logical expression is true.

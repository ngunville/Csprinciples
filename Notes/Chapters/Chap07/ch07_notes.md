## Chapter 7 Notes

Computers can do things as many times as you want.
- for is used to repeat code
- range can creat a list of numbers

## Repeating with Numbers

- for loops will use a varible and make it take on each of the values in a list one at a time
- the list holds the values in order

This code...
```

sum = 0  # Start out with nothing
things_to_add = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for number in things_to_add:
    sum = sum + number
print(sum)

```
is a good example of a for loop. 

The sum starts at 0 and the numbers 1-10 are added to it.
Then, it adds all of the numbers and stops.
This is way fatser that manualy counting this out and could be used to add bigger numbers.

If you indent the print command the same aount as the line above it, you will have a for loop that contains both. This will print every time it does something. 

If you have 0 indents, you will recive an error.

# What is a list?
lists in Python are enclosed in brackets [] and are similar to lists you use everyday.
Lists can be grouped together and you can do things to them as one entity.

You should use better varible names to make the code easier to understand and more 'friendly'
Varibles can also change due to the previous imput.
It is very important that you indent if you want it to be contained in a loop.

### Range
- This function is used to loop over a sequence of numbers.
- It is called when you put in a single positive integer.
- It will give you back all of the integers from on eto one less that the number.

- when you give a range function two values like this code

```

for number in range(1, 10):
    print(number)

```

It will give you all of the integers from the first to the second numbers. 
It will not include the second number but it will include the first.


You can set a range to a list and have the code to something to each of those numbers.

# Patterns
there is a pattern called the accumulator pattern. You can accumilate values. There are five steps to this process.

1. Set the accumulator varible to its inital value. The one that stands if there is no data.
2. Collect all the data for processing.
3. Use a for loop while running the code so the varible updates. 
4. Combine all of the peices into the accumulator.
5. Do something with the final result.

You can also use range to give you all of the values between certain numbers counting by twos or any other number. 
You do this by putting in three values for range.
You can do the same thing counting down by putting the last value to a negative.
You can use this to do things like find all of the odd and even numbers in a data set.


# Print Statements
If you put prints into the for loop, they print many times and the result is cluttered.
print statments are used to show the final result. 
There can be as many as you need in your program.

# Summary
- Accumulator Pattern - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to sum a list of numbers.
- Body of a Loop - The body of a loop is a statement or set of statements to be repeated in a loop. Python uses indention to indicate the body of a loop.
- Indention - Indention means that the text on the line has spaces at the beginning of the line so that the text doesn’t start right at the left boundary of the line. In Python indention is used to specify which statements are in the same block. For example the body of a loop is indented 4 more spaces than the statement starting the loop.
- Iteration - Iteration is the ability to repeat a step or set of steps in a computer program. This is also called looping.
- List - A list holds a sequence of items in order. An example of a list in Python is [1, 2, 3].
- Loop - A loop tells the computer to repeat a statement or set of statements.
 There are also some key words. They are 
1. def
2. for
3. print
4. range

If you do not know what these mean, go [here](https://www.openbookproject.net/books/StudentCSP/CSPRepeatNumbers/ch7_summary.html)

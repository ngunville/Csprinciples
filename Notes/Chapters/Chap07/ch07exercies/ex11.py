# Step 1: Initialize accumulator
sum = 0  # Start out with nothing

  # Step 2: Get data
numbers = range(0, 21, 2)

  # Step 3: Loop through the data
for number in numbers:
      # Step 4: Accumulate
    sum = sum + number

  # Step 5: Process result
final = sum + sum
print(final/2)



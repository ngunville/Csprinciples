### Deffinitions 
# asymmetric ciphers
 - use two keys, a public and private. one to encrypt messages and another to decrypt them. 
# Authentication
 - this is used to identify a user or device. It is often used before allowing acess to information. 
# bias
 - this describes systemic and repeatable errors that make outcomes that are unfair towards certain people. 
# Certificate Authority (CA)
 - this is a trusted thing that issues Secure Sockets Layer certifiactes. These are data files that link something with a public key. They are used by web browsers to make sure content is sent. 
# citizen science
 - research conducted with perticipation from the public (nonprofessional scientists) 
# Creative Commons licensing
 - This license give everyone a standardized way to grant permision to the public to use their work uder copywright law.
# crowdscourcing
 - a method of obtaining information from large amounts of people, usually on the internet.
# cybersecurity
 - protecting critical systems and sensitive info from digital attacks. 
# data mining
 - the process of using computers and automation to search through large sets of data for patterns and trends, making these into predictions and insights.
# decrypition
 - a process that transforms encrypted info back into its origional format.
# digital divide
 - this refers to the gap between diffrent demographics and regions that have diffrent levels of acess to modern information and technology. 
# encyrption
 - a process that transforms data to hide or obscure its meaning. 
# intelectual property (IP)
 - this is a computer code or program that is protected by law against copying, theft, or other use that is not allowed by the owner.
# keylogging
 - a type of surveillance technology used to monitor and record keystrokes on a computer.
# malware
 - Malicious software is a file or code this infectes, explores, steals, or conducts anything that the attacker wants. 
# multifactor authentication
 - a method to authenticate a user in which they have to provide two or more verification factors to gain access to a resource.
# open access
 - free access to information and unrestricted use of electronic resources for everyone.
# open source
 - Code that is designed to be publicly accessible.
# free software
 - software that respects users' freedom an community. This is a matter of libertym not price.
# FOSS
 - Free and open scource software allows users and programmers to edit, modify, or reuse the source code. Free means that is has no constraints or copywrights. 
# PII (personally identifiable information)
 - any representation of information that permits the identity of an individual to whom the information applies to be reasonably inferred by either direct or indirect means.
# phishing
 - a scam where thieves attempt to steal personal or financial account information by sending deceptive electronic messages that trick unsuspecting consumers into disclosing personal information.
# plagiarism
 - using another person's source code and claiming it as your own.
# public key encryption
 - a method of encrypting or signing data with two different keys and making one of the keys, the public key, available for anyone to use. 
# rogue access point
 - a device not sanctioned by an administrator, but is operating on the network anyway.
# targeted marketing
 - aims to promote a product among the target audience and establish a customer base in the target market.
# virus
 - A computer program that can copy itself and infect a computer without permission or knowledge of the user.



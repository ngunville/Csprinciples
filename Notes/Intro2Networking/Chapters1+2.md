# Intro To Networking Notes
### Goal: To provide a basic understanding of the structure of the internet. This book should be able to be understood by anyone trying to learn about the internet.
## Chapter 1
### 1.1 Communicating at a distance
- In the past, to communicate between spaces, people used wires and microphones to hear each other. This is doable with very few people but would not work with thousands of people.
- This was how olden-day telephones work. There was an operator who connected lines for people to speak into. This became a lot more complicated over longer distances. 
### 1.2 Computers Communicate Diffrently
- Computers can communicate just like people in todays day-and-age. They can send messages to each other that are either short, medium, or long. 
- Short messages: Check if a computer is online
- Medium messages: A picture or a long email
- Long messages: Movies or a peice of software
When computers wanted to communicate, they would have to have lines that connected them. This would be very expensive because the lines would be on 24 hours a day. 
### 1.3 Early Wide Area Store-and-Forward Networks
- In the past, computers could hop messages through each other as long as the intermediates agreed to contain the message. 
- This helped send data long distances. 
### 1.4 Packets and Routers
- In the 1960s they came up with a way to break messages into smaller parts called packets it became widely used in the 80s. 
- This reduced the amount of storage that was taken to send messages.
- This made most compaines incorperate it into their servers.
- They dedicated routers to route packets toward their final destination
### 1.5 Adressing and Packets
- Each computer is given an address so other computers can find the best way to send the messages
- each packet has to be given an address for where to send it. 
### 1.6 Putting It All Together
- All of these combined create the 'internet'. 
- Diffrent packets of the same message can be sent on diffrent paths but all end up in the same place. 
- Each packet has an offset so that the the end computer can know how to order the packets
- Internet comes from interworking which means all of the things are working together
### 1.7 Glossary
address: A number that is assigned to a computer so that messages can be routed to the computer.
hop: A single physical network connection. A packet on the Internet will typically make several “hops” to get from its source
computer to its destination.
LAN: Local Area Network. A network covering an area that is
limited by the ability for an organization to run wires or the power
of a radio transmitter.
leased line: An “always up” connection that an organization
leased from a telephone company or other utility to send data
across longer distances.
operator (telephone): A person who works for a telephone company and helps people make telephone calls.
packet: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the
Internet. The typical maximum packet size is between 1000 and
3000 characters.
router: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the
best outbound link to speed the packet to its destination.
store-and-forward network: A network where data is sent
from one computer to another with the message being stored
for relatively long periods of time in an intermediate computer
waiting for an outbound network connection to become available.
WAN: Wide Area Network. A network that covers longer distances, up to sending data completely around the world. A WAN
is generally constructed using communication links owned and
managed by a number of different organizations.
### 1.8 Questions 
1. b
2. d
3. d
4. c
5. a
6. d
7. a
8. c
## Chapter 2
### 2.1 The Link Layer
- The link layer is for connecting your computer to it's local networkand moving data across a single hop.
- This needs to solve two problems
1. how to encode and send data across the link
2. Agree on the frequinces of light to be used and how fast to send the data
- COmputers know if others computers want to send data at the same time by using CSMA/CD
- When your computer finishes sending a packet of data, it pauses
to give other computers that have been waiting a chance to send
data.
### 2.2 The Interwork Layer (IP)
- the router makes its best
guess as to how to get your packet closer to its destination.  
- When the packet reaches the last link in its journey, the link layer knows exactly
where to send your packet.
- Becasue your packet gets closer and closer to the final destination
- Routers exchange special messages to inform each other about any kind of traffic delay or network outage
### 2.3 The Transport Layer (TCP)
- The interwork layer is simple and complex.
- Sometimes packets get lost or delayed. 
- SOmetimes the packets arive out of order
- This can be solved by the packets each having the scource computers adress and where it fits into the overall message. 
- The sending computer keeps a copy of the message untill the message is fully sent
- The amount of data that the source computer sends before waiting for an acknowledgement is called the “window size”.
### 2.4 The Application Layer
- The Link, Internetwork, and Transport layers work together to quickly and reliably move data between two computers across a shared network of networks.
- 2 halves
1. called the server, runs on destination computer and waits for imcoming network 
2. Client, runs on scource computer, making connections to web servers and displays pages and documents stored on web servers
### 2.5 Stacking the Layers
- we usually run all four layers 
- if you want to write your own networked application, you would only talk to the transport layed and not worry about the interwork and link layers
### 2.6 Glossary
- client: In a networked application, the client application is the
one that requests services or initiates connections.
- fiber optic: A data transmission technology that encodes data
using light and sends the light down a very long strand of thin 
glass or plastic. Fiber optic connections are fast and can cover
very long distances.
- offset: The relative position of a packet within an overall message or stream of data.
- server: In a networked application, the server application is the
one that responds to requests for services or waits for incoming
connections.
- window size: The amount of data that the sending computer is
allowed to send before waiting for an acknowledgement.
### 2.7 Questions
1. c
2. a
3. b
4. a
5. b
6. a
7. d
8. a
9. c
## Application
## Transport
## Interwork
## Link

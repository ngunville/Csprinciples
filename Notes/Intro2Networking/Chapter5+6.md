# The Domain Name System
- The Domain Name system lets you access websites by their domain name
so you dont have to remember the ip addresses
### 5.1 Allocating Domain Names
- ICANN also assigns 2 letter country code domain names, like .us,.za, and .jp
- Once a domain name is assigned to an organization, the controlling organization is allowed to assign subdomains within the domain
### 5.2 Reading Domain Names
- The left part is the network number and very broad, the right part narrows it down
- For Domain Names, the oppisite is true
- It starts with the personal things and then goes into the more general things
- the very right is the most general
### 5.3 Summary 
- allow end users to use symbolic names for servers instead of numeric Internet Protocol addresses
- you can buy a domain name
### 5.4 Glossary
- DNS: Domain Name System. A system of protocols and servers
that allow networked applications to look up domain names and
retrieve the corresponding IP address for the domain name.
- domain name: A name that is assigned within a top-level domain. For example, khanacademy.org is a domain that is assigned
within the “.org” top-level domain.
- ICANN: International Corporation for Assigned Network Names
and Numbers. Assigns and manages the top-level domains for
the Internet.
- registrar: A company that can register, sell, and host domain
names.
- subdomain: A name that is created “below” a domain
name. For example, “umich.edu” is a domain name and
both “www.umich.edu” and “mail.umich.edu” are subdomains
within “umich.edu”.
- TLD: Top Level Domain. The rightmost portion of the domain
name. Example TLDs include “.com”, “.org”, and “.ru”. Recently,
new top-level domains like “.club” and “.help” were added.
## Questions
1. a
2. c
3. c
4. c
# Chapter 6: The Transport Layer
- above the interwork layer
- not not attempt to gaurente delivery of packets
- almost perfect but sometimes packets can be lost
- handles reliability and message reconstruction
- adds a small amount of data to each packet to help solve problems
### 6.1 Packet Headers
- link header is removed when packet is recieved on one link 
- IP and TCP stay with the packet 
- TCP headers indicate where the data belongs 
### 6.2 Packet Reassembly and Retransmission
- as destination recives packets, it looks at the offset 
- to avoid overwhelming the network, it only sends a certain amount of data before waiting for an aknoledgement
- this is called the window size
- to make sure the computers dont wait forever, the destination computer keeps track of the amount of time since it recived the last packet
### 6.3 The Transport Layer in Operation
- sending computer must hold all data untill the other aknoledges that is has got it,
- then, it can discard 
- Thn, the transport layer reconstructs the data
### 6.4 Application Clients and Servers
- initiates the connection is called a client and the responder is the server
### 6.5 Server Applications and Ports
- when client wants to connect with a remote computer, the connection must be made to the corret server on that computer
- Ports allow a client to choose which server it wants to interact with
- all extentions have the same phone number(Ip address) but they all have a diffrent port number
### 6.6 Summary
- Transport layer compensates for the fact that the link and interwork layer might lose data
- they reasemble, or retransmit the data
### 6.7 Glossary
- acknowledgement: When the receiving computer sends a notification back to the source computer indicating that data has
been received.
- buffering: Temporarily holding on to data that has been sent or
received until the computer is sure the data is no longer needed.
- listen: When a server application is started and ready to accept
incoming connections from client applications.
- port: A way to allow many different server applications to be
waiting for incoming connections on a single computer. Each
application listens on a different port. Client applications make
connections to well-known port numbers to make sure they are
talking to the correct server application.
### 6.8 Questions
1. c
2. c
3. b
4. b
5. d
6. a
7. c
8. client
9. c
10. 143


# Chapter 7: Application Layer
- the top most layer 
- where networked software operate
### 7.1 Client and Server Applications
- Two parts required for networked application to function
- To browse a web address, you must have a web application running
- when you type in an address, it connects you to the appropriate web server, and shows you pages
- The layers exchage data with applications when called upon
### 7.2 Application Layer Protocols
- There are a set of rules for applications to follow
- These protocols can be intricate
- https must be typed at the beggining of a url if you are usiong the http protocol
- Http is relativly simple and is descirbed by 6 diffrent documents
### 7.3 Exploring the HTTP protocol
- The telnet application was built before the first TCP/IP network
- port 80 is where you would find a web server
- 200 means that things went well, 404 means that the document was not found, 301 means the document was moved to a new location
- 2xx= sucess, 4xx= client failure, 5xx= server failure
### 7.4 The IMAP protocol for Retrieving Mail
- when you computer is off, your mail is sent to a server and stored untill is gets turned back on
- IMAP is more complicated and can noit be faked by telnet
- Messages sent by the client are noit descriptive and are not ment to be seen
- These are precisely formatted and sent in a correct order 
### 7.5 Flow Control
- the transport layer will pause and wait for a aknoledgement
- the ability to start and stop the sending application is called flow control
- The applications are not responsible for this
### 7.6 Writing Networked Applications
- Making connections and sending data is relativily easy
- This is becasue the lower layers work so seemlessly to solve problems
### 7.7 Summary
- lower three layers makes it so the applications running on the application layer can focus on problems that need to be solved
- This makes it simple to build networked applications
### 7.8 Glossary
- HTML: HyperText Markup Language. A textual format that
marks up text using tags surrounded by less-than and
greater-than characters. Example HTML looks like: <p>This
is <strong>nice</strong></p>.
- HTTP: HyperText Transport Protocol. An Application layer protocol that allows web browsers to retrieve web documents from web
servers.
- IMAP: Internet Message Access Protocol. A protocol that allows
mail clients to log into and retrieve mail from IMAP-enabled mail
servers.
- flow control: When a sending computer slows down to make
sure that it does not overwhelm either the network or the destination computer. Flow control also causes the sending computer
to increase the speed at which data is sent when it is sure that
the network and destination computer can handle the faster data
rates.
- socket: A software library available in many programming languages that makes creating a network connection and exchanging data nearly as easy as opening and reading a file on your
computer.
- status code: One aspect of the HTTP protocol that indicates
the overall success or failure of a request for a document. The
most well-known HTTP status code is “404”, which is how an HTTP
server tells an HTTP client (i.e., a browser) that it the requested
document could not be found.
- telnet: A simple client application that makes TCP connections
to various address/port combinations and allows typed data to be
sent across the connection. In the early days of the Internet, telnet was used to remotely log in to a computer across the network.
- web browser: A client application that you run on your computer
to retrieve and display web pages.
- web server: An application that deliver (serves up) Web pages
### Questions 
1. a
2. b 
3. d
4. d
5. a
6. b
7. d
8. a
9. d
10. d
11. a
12. d
13. d 
14. c
15. d
16. a
# Chapter 8: Secure Transpot Layer
### 8.1 Encrypting and Dectrypting Data
- in the old days, armies used ciphers to encode messages, the person on the other end just has to know how to decode the cypher
### 8.2 Two Kinds of Secrets
- one way to encyrpt messages is to use a shared secret like a sentence
- This makes it hard to decrypt without the secret
- This is not a good situation when you are doing it with networks becasue they are better at decrypting
- This is why the public-private key was implemented
### 8.3 Secure Sockets Layer
- Security was added to the internet 20 years ago
- security was added so that there was no need to chage the others layers to make the SSL work
- They just had to request that a connection be encrypted
### 8.4 Encrypting WEb Browser Traffic
- Web browsers use the URl conventions that replace http with https
- Your browser will have a lock icon in the adress bar when it is secured
- This happens almost automaticaly 
### 8.5 Certificates and Certificate Authorities
- When using public/private key encryption, you dont know if the public key is from the place where it says it is
- This is why you take is to the Certificate Authority
- This gives you an ok
### 8.7 Glossary
- asymmetric key: An approach to encryption where one (public)
key is used to encrypt data prior to transmission and a different
(private) key is used to decrypt data once it is received.
- certificate authority: An organization that digitally signs public
keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.
- ciphertext: A scrambled version of a message that cannot be
read without knowing the decryption key and technique.
- decrypt: The act of transforming a ciphertext message to a plain
text message using a secret or key.
- encrypt: The act of transforming a plain text message to a ciphertext message using a secret or key.
- plain text: A readable message that is about to be encrypted
before being sent.
- private key: The portion of a key pair that is used to decrypt
transmissions.
- public key: The portion of a key pair that is used to encrypt
transmissions.
- shared secret: An approach to encryption that uses the same
key for encryption and decryption.
- SSL: Secure Sockets Layer. An approach that allows an application to request that a Transport layer connection is to be en-crypted as it crosses the network. Similar to Transport Layer Security (TLS).
- TLS: Transport Layer Security. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer
(SSL).
### Questions
1. a
2. 
3. 
4. a
5. 
6. d
7. c
8. a
9.  
10. c
11. 
12. c
13. 

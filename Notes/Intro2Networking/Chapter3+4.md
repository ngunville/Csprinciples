# Chapter 3: The Link Layer
- The lowest layer is called the link layer
- it is closest to the physical network media
- usually transmits using a wire, fiber optic cable, or radio signal 
### 3.1 Sharing the Air
- when device is using internet, it is sending and reciving data through a small radio
- can u=only go about 900 feet 
- this also means that a rouge computer could take packets 
- every wifi has a serial number
- when you first connect to a wifi, it has to discover the MAC address
- sends a massage to the network
- sends a message back to the computer
- want to brodcast address as little as possible becasue every computer process all messages
### 3.2 Courtesy and Coordination
- 1 teqnique is called a Carrier Sense 
1. listen for a transmission, if it hears nothing
2. now your computer can send data
### 3.3 Coordination in Other Link Layers
- when things need to be very efficient for a long amount of time
- computer must have a token in order to transmit data
- just like a "speaking stick"
- if you want to send a packet, you have to wait untill you have a token
- this approach is best suited  when using a medium like a fiber optic cable
### 3.4 Summary
- Link= lowest layer
- one bennifit is is that it can ignore issues above the link layer
- this means it can focus on moving data across a single hop
- link layer works well
### 3.5 Glossary
- base station: Another word for the first router that handles your
packets as they are forwarded to the Internet.
- broadcast: Sending a packet in a way that all the stations connected to a local area network will receive the packet.
- gateway: A router that connects a local area network to a wider
area network such as the Internet. Computers that want to send
data outside the local network must send their packets to the
gateway for forwarding.
- MAC Address: An address that is assigned to a piece of network
hardware when the device is manufactured.
- token: A technique to allow many computers to share the same
physical media without collisions. Each computer must wait until
it has received the token before it can send data.
### Questions
1. a
2. c
3. a
4. b
5. a
6. d
7. d
## I was not sure what you wanted truned into binary 
## this is probably worng
## 0000(1111):0010(1010):(1011)0011:0001(1111):(1011)0011:0001(1010)
# Chapter 4: The Interworking Layer
This layer figures out how to move data across a country or the world. 
The packet passes through many stations. 
### 4.1: Internet Protocol Addresses
- second to last layer
- cant use the link layer address because their is no relationship between the locations and the link layer. Because of the portable computers.
- Two diffrent types of versions of Ip addresses
1. IPv4: old, consists of four numbers seperated by dots
- ex: 212.78.1.25
- numbers can only be 0-255
2. IPv6
- longer, contains hexidecimal digits as well as numbers
The IP addresses can be broken into two parts
1. first is the Network Number, first two numbers
- This is like a zip code, multiple computers in a single internet connection can use
- This means all computers appear to the rest of the internet as just thier network number
- This makes the whole system a lot less complicated
2. The second is the Host Identifier
- The last two numbers in the address
- helps single out computers
### 4.2 How Routers Determine the Routes
- When a router is connected to the internet, it dosen't know all of the routes
- discovers routes as it encounters packets
- Mapping is called a Routing Table
### 4.3 When Things Get Worse and Better
- sometimes the outbound links fails. When this happens the router discards all of the entries, as more packets arrive, the router discovers all of the route again
- when the broken link is repaired the router inproves itself
### 4.4 Determining Your Route
- The internet does not know in advance the route your packets will take. 
- each place only knows that next place until the very end.
- We can trace a route to find a pretty good guess of where it will go
- Any info in a single router is imperfect
- Sometimes a traceroute can take up to a minute or two. 
- trace routes that are run undersea show you how fast undersea data travels
### 4.5 Getting an IP Address
- Since network numbers indicate a physical connection, a compouter must get a new IP address when it moves. 
- The link address is set when a computer is made and never changes but the IP address does
- "Dynamic Host Configuration Protocol" gives the ability to mave from one network to another
- SOmetimes your computer will have the same address as another so it thinks it is assigned to that
### 4.6 A Diffrent Kind of Address Reuse
- If an address starts with a 192.168 means that it is non routable. 
- When one of these tries to send packets, the gateway router replaces it with an acucual routable address
- this helps routers conserve the real, routable addresses and use the non-routable ones for workstations that move networks
### 4.7 Global IP Address Allocation
- at the top of IP address allocations are 5 Regional Internet Registratries, each allocates for a major geographic area
- these cover the entire world
- More computers are being made so the newer IP addresses are 128 bits instead of 32
### 4.8 Summary
- IP layer is not 100 percent reliable, but mostly accurate
- This layer just pushes the packets and problems on to the transport layer which sorts out all of the problems
### 4.9 Glossary
- core router: A router that is forwarding traffic within the core of
the Internet.
- DHCP: Dynamic Host Configuration Protocol. DHCP is how a
portable computer gets an IP address when it is moved to a new
location
- edge router: A router which provides a connection between a
local network and the Internet. Equivalent to “gateway”.
- Host Identifier: The portion of an IP address that is used to
identify a computer within a local area network.
- IP Address: A globally assigned address that is assigned to a
computer so that it can communicate with other computers that
have IP addresses and are connected to the Internet. To simplify
routing in the core of the Internet IP addresses are broken into
Network Numbers and Host Identifiers. An example IP address
might be “212.78.1.25”.
- NAT: Network Address Translation. This technique allows a single
global IP address to be shared by many computers on a single
local area network.
- Network Number: The portion of an IP address that is used to
identify which local network the computer is connected to.
- packet vortex: An error situation where a packet gets into an
infinite loop because of errors in routing tables
- RIR: Regional Internet Registry. The five RIRs roughly correspond
to the continents of the world and allocate IP address for the major geographical areas of the world.
- routing tables: Information maintained by each router that
keeps track of which outbound link should be used for each
network number
- Time To Live (TTL): A number that is stored in every packet
that is reduced by one as the packet passes through each router.
When the TTL reaches zero, the packet is discarded.
- traceroute: A command that is available on many Linux/UNIX
systems that attempts to map the path taken by a packet as it
moves from its source to its destination. May be called “tracert”
on Windows systems.
- two-connected network: A situation where there is at least two
possible paths between any pair of nodes in a network. A twoconnected network can lose any single link without losing overall
connectivity.
### 4.10 Questions
1. a
2. 
3. b
4. c
5. a
6. c
7. b
8. 
9. 
10. a
11. d
12. b
13. 
14. a
15. a
16. b
17. a
18. d
19. a
20. c
21. 
22. c
# Binary Numeber; inet 10011110.111011.11100001.1110011 

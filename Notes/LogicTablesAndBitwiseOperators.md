# Bitwise Operators

|Operator|Type                |Description|Example|
|--------|--------------------|-----------|-------|
|&       |bitwise AND         |This returns 1 one only if the two bits are ones. If not, this returns a 0|0011&1111=0011|
|\|      |bitwise OR          |This returns a 1 if either contains a 1. If they both have a 0, it will return a 0|1100\|1010=1110|
|^       |bitwise exclusive OR|This returns a 1 when one of the bits is a one. If both or none,it will return a 0|0011^1001=1010| 
|<<      |shift left          |This shifts the 1st bit left by the value of the second bit|1101<<0010=0100|
|>>      |shift right         |This shifts the 1st bit to the right by the second's bits value|1000>>0011=0010| 
|~       |bitwise complement/not  |This inverts the bits of the first value, it converts it into a 32 bit number|0000~1111=1111|
# Logic tables
# &
|a   |b   |a and b|
|----|----|-------|
|0   |0   |0      |
|0   |1   |0      |
|1   |0   |0      |
|1   |1   |1      |
# |
|a  |b  |a or b|
|---|---|------|
|0  |0  |0     |
|0  |1  |1     |
|1  |0  |1     |
|1  |1  |1     |
# ~
|a  |Result|
|---|------|
|0  |1     |
|1  |0     |
# <<
|A  |B  |Output|
|---|---|------|
|1010|0001|0100|
|1101|0010|0100|
|0001|0011|0100|
|1001|0100|1000|
# >>
|A  |B  |Output|
|---|---|------|
|1010|0001|0101|
|1101|0010|0011|
|1000|0011|0010|
|1001|0100|0001|
# Bitshift
![image](Shift.png)

# Quiz 1
## Question 7
Every night at 12:00am, you notice that soem files are being uploaded from your PC device to an unfamilier website without your knoledge. You check the file and find out that these files are from the days work at the office. WHat is this an example of?
1. Phishing attack
2. Keylogger attack
3. computer virus
4. malware insertion point
### correct answer = 3
### I choose 4
I choose this because I thought that the website was an insertion point.
The correct answer is right because the insertion point dosen't do anything but allows the virus to enter.
# Quiz 2
## Question 4
What is displayed using the following display(expression) abstractions?
- List1<- [11,35,6]
- DISPLAY(List1[2])
1. error
2. 35
3. 11
4. 6
- I choose d becasue python starts with 0 and the third number is assigned a 2
- The correct answer is b beasue this is psudocode and it starts with 1, therefore, the second value is 35
## Question 5
Consider the following code segment, which uses the varibles a and c.
- a<- 3
- a<- a+5
- c<- 3
- a<-c+a
- DISPLAY(a)
- DISPLAY(c)
What is displayed when running the code segment
1. 3 3
2. 3 8
3. 11 3
4. 3 11
- I choose 4 due to a lack of care, I thought that c was displayed earlier
- The correct answer is 3 becasue a was displayed before c
# Quiz 3
## question 1
COnsider this sequence- 10100101100000100110110(24terms) how many bytes is this
1. 1
2. 2
3. 3
4. 4
- I choose 2 becasue i thought that their were 16 bits in a byte
- The correct answer is 3 becasue their are 8 bits in a byte
## question 2
How many bits are in four bytes
1. 16
2. 32
3. 64
4. 128
- I choose 3 becasue I thought that there were 16 bits in a byte
- The answer is 2 becasue there are 8 bits in a byte
## Question 3
Digital alarm clocks display information and visual indicators to help people wake up on time. WHich of the indicators could represent a single bit of imformation(Choose 2)
1. WHat month it is
2. Pm/Am
3. current hour
4. The unit of temp f/c
- I choose just 2 becasue I didnt see the select 2
- The right answer is 2 and 4 becasue One bit can be i of two things so there can only be 2 possibilities
## question 6
The same task is preformed through sequential and parallel computing models. The time thaken by the first was 30 senconds, the parallel took 10 seconds. What will the speedup parameter be.
1. .3333
2. 3
3. 300
4. 20
- I choose 2 becaue I associated larger with faster
- In reality, the answer is 1 becasue it represents a third of the time
## Question 8
A small team of scientists are working on monitering the sucess rate of catipillars to butterflies, which of the following best explains why citizen science is useful for the project(Choose 2)
1. They are more able to cont a bigger number of things over a larger area
2. The social media can be used to find people interested
3. The gp are more likely to identify specific types of insects
4. COunting is too hard for citizen scientists
- I choose just 1 becasue I didnt see the select 2
- The correct answer is 1 and 2 becasue the problem said select 2
## Question 10
Rewrite 1011011001101010(16 terms) with a space at the end of each byte
1. 1011 0110 0110 1010
2. 10 11 01 10 01 10 10 10
3. 1011011001101010
4. 10110110 01101010
- I choose 3 becasue I thougth that there were 16 bits in a byte
 The answer is 4 becasue their are 8 bits is a byte
# Quiz 4
## question 8
COnsider the following procedure called mystery, it is intended to display the number of times a number target appears in a list
- count <- 0
- For each n in list
- if (n=target)
- count<-count +1
- else
- count<- 0
- display (count)
Which of the followign describes the behavior of the procedure
1. It correctly displays teh count if the target is not in the list
2. It correctly displays the correct value for count
3. It correctly displays the count if teh target appears once at the end of the list
4. The program is always right
- I choose 1 and 4 becasue I thought these were right
- Now I know that the answers are 1 and 3 besause, using the process of elimination, the others are not valid
## Question 9
The diagram below shows a circut of composed logic gates, each takes two inputs and produces a single output, what will the algorithm produce
- True) (a)
- or                true
-             and              b
-                       or
-                        Result
1. True when bith are only true
2. True when a is true and b is false
3. Always true regardless of a and b
4. always false regardless of a and b
- i choose 1 because it worked when i plugged it is
- the answer is 3 becaue it is always true no matter when you plug in.

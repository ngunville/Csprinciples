from gasp import *
from random import randint

def place_player():
    global x, y, c, key
    x = randint(0,64)
    y = randint(0,48)
    c = Circle((10 * x + 5, 10 * y + 5), 5, filled = True)

def move_player():
    global x, y, c, key
    key = update_when('key_pressed')
    if key == 'q':
        y = y + 1
        x = x - 1
        move_to(c, (10 * x,10 *  y))
    elif key == 'e':
        y = y + 1
        x = x + 1
        move_to(c, (10 * x,10 * y))
    elif key == 'z':
        y = y - 1
        x = x - 1
        move_to(c, (10 * x,10 * y))
    elif key == 'c':
        y = y - 1
        x = x + 1
        move_to(c, (10 * x,10 * y))
    elif key == 'w':
        y = y + 1
        move_to(c, (10 * x,10 * y))
    elif key == 'a':
        x = x - 1
        move_to(c, (10 * x,10 * y))
    elif key == 'd':
        x = x + 1
        move_to(c, (10 * x,10 * y))
    elif key == 's':
        y = y - 1
        move_to(c, (10 * x,10 * y))
    elif key == 'x':
        move_to(c, (10 * x,10 * y))
    elif key == 'r':
        y = random.randint(0, 48)
        x = random.randint(0, 64)
        move_to(c, (10 * x,10 * y))
def place_robot():
    global rx, ry, rc
    rx = randint(0, 64)
    ry = randint(0, 48)
    rc = Circle((10 * rx + 5,10 * ry + 5), 5, filled = False)
def place_robot2():
    global 2rx, 2ry, 2rc
    2rx = randint(0,64)
    2ry = randint(0,48)
    2rc = Circle((10 * 2rx + 5,10 * 2ry + 5), 5, filled = False)
def move_robot():
    global rx, ry, rc
    if rx < x:
        rx = rx + 1
    if rx > x:
        rx = rx - 1
    if ry > y:
        ry = ry - 1
    if ry < y:
        ry = ry + 1
    move_to(rc,(10 * rx,10 * ry))
def move_robot2():
    global 2rx, 2ry, 2rc
    if 2rx < x:
        2rx = 2rx + 1
    if 2rx > x:
        2rx = 2rx - 1
    if 2ry > y:
        2ry = 2ry - 1
    if 2ry < y:
        2ry = 2ry + 1
    move_to(2rc,(10 * 2rx,10 * 2ry))
def player_caught():
    global rx, ry, x, y, finished, key2, msg 
    if x == rx and y == ry:
        msg = Text("Game over", (300, 200), size=70)
        sleep(5)
        remove_from_screen(msg)
        Text("Press V", (300, 300), size=50)
        key2 = update_when('key_pressed')
        if key2 == 'v':
            finished += 2
    if x == 2rx and y == 2ry:
        msg = Text("Game over", (300, 200), size=70)
        sleep(5)
        remove_from_screen(msg)
        Text("Press V", (300, 300), size=50)
        key2 = update_when('key_pressed')
        if key2 == 'v':
            finished += 2
begin_graphics()
place_robot()
place_robot2()
place_player()


finished = 0
while finished == 0: 
    move_player()
    move_robot()
    
    player_caught()

def questions(amount):
  from random import randint  # import randint for use of the randint function
  score = 0
  print (f"Ok, you have {amount} questions to answer \n")
  print ("\n Please type in the Capitals of these states. \n")
  print ("Please capitalize the first letter when typing out capitals.\n")
  states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakoda', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakoda', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
  capitals = ['Montgomery', 'Juneau', 'Phoenix', 'Little Rock', 'Sacramento', 'Denver', 'Hartford', 'Dover', 'Tallahassee', 'Atlanta', 'Honolulu', 'Boise', 'Springfeild', 'Indianapolis', 'Des Moines', 'Topeka', 'Frankfort', 'Balton Rouge', 'Augusta', 'Annapolis', 'Boston', 'Lansing', 'Saint Paul', 'Jackson', 'Jefferson City', 'Helena', 'Lincoln', 'Carson City', 'Concord', 'Trenton', 'Santa Fe', 'Albany', 'Raleigh', 'Bismark', 'Columbus', 'Oklahoma City', 'Salem', 'Harrisburg', 'Providence', 'Columbia', 'Pierre', 'Nashville', 'Austin', 'Salt Lake City', 'Montpelier', 'Richmond', 'Olympia', 'Charleston', 'Madison', 'Cheyenne']
  wrongques = ""
  while amount >= 1:
    randpair = randint(1, len(capitals) + 1)
    quesinput = input(states[randpair] + "\n")
    quesanswer = capitals[randpair]
    if quesinput == quesanswer:
      print("\n Correct!")
      score += 1
    else:
      print("\n Incorrect. \n")
      wrongques = wrongques + "\n" + states[randpair]
    states.remove(states[randpair])
    capitals.remove(capitals[randpair])
    amount = amount - 1
  print(f"\n Your score is {score}.")
  while wrongques:
    print("\n The states with capitals you do not know are: \n" + wrongques)
    break
  print("Check your score")
  return
import os  # Importing os to use clear function
import time  # Importing time to use sleep function
amount = int(input("How many questions do you want to be asked? (Remember there are only 50 states) \n"))
# Ask user for amount of questions
questions(amount)  # Call function questions with parameter amount
print("Round 2 is starting in 5 seconds!!")
time.sleep(5) # Wait 5 seconds before starting next line of code
os.system('clear') # Clear text from console
amount = int(input("How many questions do you want to be asked? (Remember there are only 50 states) \n"))
# Ask user for amount of questions
questions(amount)
ready = (input("\n Do you want to play another round? \n"))
# Get input to see if User wants to play again
if str(ready) == 'yes' or str(ready) == 'Yes' or str(ready) == 'y' or str(ready) == 'Y':
# check if the user answered yes for variable ready
    time.sleep(5)
    # Wait 5 seconds before starting next line of code
    os.system('clear')
    # Clear text from console
    amount = int(input("How many questions do you want to be   asked? (Remember there are only 50 states) "))
    # Ask user for amount of questions
    questions(amount)
    # Call function questions with parameter amount
    print("\n Thanks for playing!")
else:
    print("\n Thanks for playing!")

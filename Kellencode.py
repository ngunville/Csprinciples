from adafruit_circuitplayground import cp
from time import sleep

# Setting everything up
cp.pixels.brightness = 0.5
cp.pixels.fill((0, 0, 0))

# Makes the proccess a constant
while True:
  # Sets background
  if cp.switch:
  	if cp.touch_A3:
    	ground = (255, 0, 0)
  	if cp.touch_A2:
    	ground = (255, 127, 0)
  	if cp.touch_A1:
      ground = (255,255,0)
    if cp.touch_A7:
      ground = (0, 255, 0)
    if cp.touch_A6:
      ground = (0, 0, 255)
    if cp.touch_A5:
      ground = (127, 63, 218)
    if cp.touch_A4:
      ground = (116 0, 218)
  # Sets moving light
	if not cp.switch:
    if cp.touch_A3:
    	light = (255, 0, 0)
  	if cp.touch_A2:
    	light = (255, 127, 0)
  	if cp.touch_A1:
      light = (255,255,0)
    if cp.touch_A7:
      light = (0, 255, 0)
    if cp.touch_A6:
      light = (0, 0, 255)
    if cp.touch_A5:
      light = (127, 63, 218)
    if cp.touch_A4:
      light = (116 0, 218)
      
  # Executes the show in ernest
  if cp.button_a:
    cp.pixels.fill(ground)
    time.sleep(0.5)
    i = 0
    while i < 9:
    	cp.pixels[i] = light
      time.sleep(0.5)
      i += 1
  # Keeps everything from getting stuck at 9
  else:
    cp.pixels.fill(0, 0, 0)

def place_player():
    Circle((10 * player_x + 5, 10 * player_y + 5), 5, filled=True)
 

def move_player():
        global player_x, player_y, player_shape
        key = update_when('key_pressed')
        if key == '6' and player_x < 63:
            player_x += 1
        elif key == '3':
            if player_x < 63:
                player_x += 1
            if player_y > 0:
                player_y -= 1
        elif key == '8' and player_y < 47:
            player_y += 1
        elif key == '4' and player_x > 0:
            player_x -= 1
        elif key == '2' and player_y > 0:
            player_y -=1
        elif key == '1':
            if player_x > 0:
                player_x -= 1
            if player_y > 0:
                player_y -= 1
        elif key == '7':
            if player_x > 0:
                player_x -= 1
            if player_y < 47:
                player_y += 1
        elif key == '9':
            if player_x < 63:
                player_x += 1
            if player_y < 47:
                player_y += 1
        move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def move_robot():
    global robot_x, robot_y, robot_shape
    if robot_x < player_x:
        robot_x += 1
    if robot_y < player_y:
        robot_y += 1
    if robot_x > player_x:
        robot_x -= 1
    if robot_y > player_y:
        robot_y -= 1
    move_to(robot_shape, (10 * robot_x + 5, 10 * robot_y + 5))

def check_collisions():
    if robot_x == player_x and robot_y == player_y:
        print("You've been caught!")
        finished = True

from gasp import *          # So that you can draw things
from random import *
begin_graphics()
player_x = randint(5, 64)
player_y = randint(5, 48)
player_shape = Circle((320, 200), 5)
robot_x = 1
robot_y = 1
robot_shape = Box((robot_x, robot_y), 5, 5, filled=True, color=color.BLACK, thickness=1)
finished = False

while not finished:
    move_player()
    move_robot()
    check_collisions()


while True:
    key = update_when('key_pressed')
    remove_from_screen(key_text)
    key_text = Text(key, (320, 240), size=48)
    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))
    if key == 'q':
        break

end_graphics()              # Finished!

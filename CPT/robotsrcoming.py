from gasp import *
from random import randint
def place_player():
    global x, y, c, key
    x = randint(5,300)
    y = randint(5,300)
    c = Circle((x, y), 5)
def move_player():
    global x, y, c, key
    key = update_when('key_pressed')
    if key == 'q':
        y = y + 4
        x = x - 4
        move_to(c, (x, y))
    elif key == 'e':
        y = y + 4
        x = x + 4
        move_to(c, (x,y))
    elif key == 'z':
        y = y - 4
        x = x - 4
        move_to(c, (x,y))
    elif key == 'c':
        y = y - 4
        x = x + 4
        move_to(c, (x, y))
    elif key == 'w':
        y = y + 4
        move_to(c, (x, y))
    elif key == 'a':
        x = x - 4
        move_to(c, (x, y))
    elif key == 'd':
        x = x + 4
        move_to(c, (x, y))
    elif key == 's':
        y = y - 4
        move_to(c, (x, y))

def place_robot():
    global rx, ry, rc
    rx = randint(5,300)
    ry = randint(5,300)
    rc = Circle((rx, ry), 5, filled=True)

def move_robot():
    global rx, ry, rc
    if rx < x:
        rx = rx + 1
    elif rx > x:
        rx = rx - 1
    elif ry < y:
        ry = ry + 1
    elif ry > y:
        ry = ry - 1
    move_to(rc, (rx, ry))
def caught():
    global rx, x, y, ry, finished, key2, msg
    if x == rx or y == ry:
        msg = Text("You were caught", (320, 240), size=48)
        sleep(1)
        remove_from_screen(msg)
        Text("Press V", (320, 240), size=48)
        key2 = update_when('key_pressed')
        if key2 == 'v':
            finished += 1
begin_graphics()
place_player()
place_robot()
finished = 0
while finished == 0:
    move_player()
    move_robot()
    caught()
    move_robot()
    move_robot()

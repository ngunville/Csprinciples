# How to Study for the AP Exam
## This is our plan to prepare for the Exam
1. Make sure you know all of the instructions
2. Study the five areas separately
- Big Idea 1: Creative Development
- Big Idea 2: Data
- Big Idea 3: Algorithms and Programming
- Big Idea 4: COmputer Systems and Networks
- Big Idea 5: Impact of Computing
![APE2.png](APE.png)
3. If you don't know a question, go back to it later
- For practice tests, leave it and save it for last, and then you can take your time to figure out the answer
4. The most important thing is to practice and study
- Take practice tests, whether online or from Jeff
- Study the resources below as well as your previous practice tests. If you get a question wrong, consider it a learning opportunity!!!
## These are some rescources that will help us study
1. [Khan Academy](https://www.khanacademy.org/computing/ap-computer-science-principles) has a course similar to our AP computer science class that can be used to review and study for the Exam. There are quizzes to test your knowledge on the different topics covered in the Exam.  
2. The [College Board](https://apcentral.collegeboard.org/courses/ap-computer-science-principles/exam/past-exam-questions) has a site with some of the past questions and some grading criteria. There are links to past years so you can see the changes in the types of questions and how they are scored. 
3. One of the best sources for the APCSP exam is [Test Guide](https://www.test-guide.com/ap-computer-science-principles-practice-exam.html)'s website. This contains 4 free practice tests and a full breakdown of the entire test. This website also gives you information such as the test is weighted at 70% of the overall credit while the Code is only 30%
- It also talks about the exam having a 66% pass rate(which is kind of high) and a 12% perfect score rate(which is considered pretty low) 
![APE.png](APE2.png)
## These are pointers to score a 3 on the exam
- To utilize the links that we have talked about, we will want to make sure that we work on the things that will be on the test using those sources. Repetition helps a lot with studying, since it ingrains what you learned into your mind. If you study a tiny bit every night or when you can it will help with memorization.  Using Khan Academy, they have lots of videos to make learning actually interesting and not boring. The college board website can help you find which videos you should watch on Khan Academy because it will give you an idea of what to study for. 


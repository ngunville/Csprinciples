# Practice Questions
1. You develop a program that lets you use different operations (addition, subtraction, multiplication, division, etc.) on a set of numbers derived from a database.

However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code. 
a. Rerunning the code
b. code tracing
c. using a code visulizer
d. using display stetements at diffrent points of code 
Correct Answer = D


2. Musicians record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?
a. The file was moved from the original location, causing some information to be lost
b. The recording was done through the lossless compression technique.
c. The song file was saved with higher bits per second
d. The song file was saved with lower bits per second

Correct answer = D
3. A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students.

Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

a. Email the prospective families that have middle-school-age children.
b. Create a report based on the student body's overall performance for a marketing pamphlet.
c. Post an interactive pie chart about the topics and scores of the passing students on the schools' website
d. Post the results of the passing students on social media sites.

Correct Answer = C

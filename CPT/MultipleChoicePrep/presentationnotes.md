# Practice Questions
1. You develop a program that lets you use different operations (addition, subtraction, multiplication, division, etc.) on a set of numbers derived from a database.

However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code. 
- [ ] Rerunning the code
- [x] code tracing
- [ ] using a code visulizer
- [ ] using display stetements at diffrent points of code 



2. Musicians record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?
- [ ] The file was moved from the original location, causing some information to be lost
- [ ] The recording was done through the lossless compression technique.
- [ ] The song file was saved with higher bits per second
- [x] The song file was saved with lower bits per second


3. A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students.

Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

- [ ] Email the prospective families that have middle-school-age children.
- [ ] Create a report based on the student body's overall performance for a marketing pamphlet.
- [x] Post an interactive pie chart about the topics and scores of the passing students on the schools' website
- [ ] Post the results of the passing students on social media sites.


# Citations
All of these qustions were from the test-guide.com practice text one.
[TestGuide](https://www.test-guide.com/quiz/ap-comp-sci-1.html) 
